package pe.edu.uni.fiis.ucalendar.dao.usuario;

import pe.edu.uni.fiis.ucalendar.model.Usuario;
import pe.edu.uni.fiis.ucalendar.service.Conexion;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class UsuarioDaoImpl implements UsuarioDao {

    public Usuario agregarUsuario(Usuario a, Connection b) {
        try {
            StringBuffer sql = new StringBuffer();
            sql.append("insert into usuario(username,correo,idusuario) values (")
                    .append("?,?,?)");

            PreparedStatement sentencia = b.prepareStatement(sql.toString());
            sentencia.setString(1, a.getUsername());
            sentencia.setString(2, a.getCorreo());
            sentencia.setString(3, a.getIdusuario());

            sentencia.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return a;
    }

    public static void main(String[] args) {
        //Conexion con = new Conexion();
        Connection c = Conexion.getConnection();
        try {
            System.out.println(c.isValid(5000));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        UsuarioDaoImpl usuariodao = new UsuarioDaoImpl();
        Usuario usuario = new Usuario("Juan", "jalvac@uni.pe", "100");
        usuariodao.agregarUsuario(usuario, c);
        try {
            //c.commit();
            c.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
        /*StringBuffer sql = new StringBuffer();
        sql.append("insert into usuario(").append("?,?,?)");
        return  a;*/
}