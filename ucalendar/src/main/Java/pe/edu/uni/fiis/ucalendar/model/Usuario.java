package pe.edu.uni.fiis.ucalendar.model;

public class Usuario {
    private String username;
    private String correo;
    private String idusuario;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getIdusuario() {
        return idusuario;
    }

    public void setIdusuario(String idusuario) {
        this.idusuario = idusuario;
    }

    public Usuario(String username, String correo, String idusuario) {
        this.username = username;
        this.correo = correo;
        this.idusuario = idusuario;
    }
}
