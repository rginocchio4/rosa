package pe.edu.uni.fiis.ucalendar.service.usuario.Impl;

import pe.edu.uni.fiis.ucalendar.dao.SingletonUsuarioDao;
import pe.edu.uni.fiis.ucalendar.model.Usuario;
import pe.edu.uni.fiis.ucalendar.service.Conexion;
import pe.edu.uni.fiis.ucalendar.service.usuario.UsuarioService;

import java.sql.Connection;
import java.sql.SQLException;

public class UsuarioServiceImpl implements UsuarioService {
    public Usuario agregarUsuario(Usuario usuario) {
        Connection connection= Conexion.getConnection();
        Usuario usur = SingletonUsuarioDao.getUsuarioDao().agregarUsuario(usuario,connection);
        try {
            connection.commit();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return usur;
    }
}