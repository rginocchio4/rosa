package pe.edu.uni.fiis.ucalendar.controller;

import pe.edu.uni.fiis.ucalendar.model.Usuario;
import pe.edu.uni.fiis.ucalendar.service.SingletonService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "UsuarioController", urlPatterns = {"/registro-usuario"})
public class UsuarioController extends HttpServlet {

    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getParameter("una");
        String correo = req.getParameter("cor");
        String idusuario = req.getParameter("idus");
        Usuario usur1 = new Usuario(username, correo, idusuario);

        SingletonService.getUsuarioService().agregarUsuario(usur1);
        resp.getWriter().write("El usuario se registró correctamente");
    }

    public static void main (String[]args){
    }
}