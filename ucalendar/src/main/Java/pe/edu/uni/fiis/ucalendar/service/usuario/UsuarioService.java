package pe.edu.uni.fiis.ucalendar.service.usuario;

import pe.edu.uni.fiis.ucalendar.model.Usuario;

public interface UsuarioService {
    public Usuario agregarUsuario(Usuario usuario);
}
