package pe.edu.uni.fiis.ucalendar.service;

import pe.edu.uni.fiis.ucalendar.service.usuario.Impl.UsuarioServiceImpl;
import pe.edu.uni.fiis.ucalendar.service.usuario.UsuarioService;

public class SingletonService {
    private static UsuarioService usuarioService = null;

    public static UsuarioService getUsuarioService() {
        if (usuarioService == null) {
            usuarioService = new UsuarioServiceImpl();
        }
        return usuarioService;
    }
}